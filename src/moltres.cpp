#include "moltres.h"

using namespace std;

Moltres::Moltres() : Pokemon { "Moltres","Legendario",90,100,90,125,85,90,500,"WOOOOOSH!" }{ 
	
	type[0]=Volador::type();
	type[1]=Fuego::type();

	strongVs[0]=Volador::strongVs();
	strongVs[1]=Fuego::strongVs();

	weakVs[0]=Volador::weakVs();
	weakVs[1]=Fuego::weakVs();
}

void Moltres::print(){
	Pokemon::printInfo();
	cout << "Tipos:_______________________" << type[0] << " - "<< type[1] << endl;
	cout << "Es fuerte contra:____________" << strongVs[0] << " y " << strongVs[1] << endl;
	cout << "Es débil contra:_____________"<< weakVs[0] << " y " << weakVs[1] << endl;
}

void Moltres::atk1(Pokemon &other){
	cout << "Moltres utiliza Sky Attack ! " << endl;//70 de daño
	other.setHP(other.getHP()-70);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}


void Moltres::atk2(Pokemon &other){
	cout << "Moltres utiliza Ember ! " << endl;//40 de daño
	other.setHP(other.getHP()-90);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}

void Moltres::atk3(Pokemon &other){
	cout << "Moltres utiliza Air Slash ! " << endl;//75 de daño
	other.setHP(other.getHP()-75);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}

void Moltres::atk4(Pokemon &other){
	cout << "Moltres utiliza Fire Spin ! " << endl;//35 de daño
	other.setHP(other.getHP()-35);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}