#include "zapdos.h"

using namespace std;

Zapdos::Zapdos() : Pokemon { "Zapdos","Legendario",90,90,85,125,90,100,666,"T S S S S S S" }{
	
	type[0]=Volador::type();
	type[1]=Electrico::type();

	strongVs[0]=Volador::strongVs();
	strongVs[1]=Electrico::strongVs();

	weakVs[0]=Volador::weakVs();
	weakVs[1]=Electrico::weakVs();
}

void Zapdos::print(){
	Pokemon::printInfo();
	cout << "Tipos:_______________________" << type[0] << " - "<< type[1] << endl;
	cout << "Es fuerte contra:____________" << strongVs[0] << " y " << strongVs[1] << endl;
	cout << "Es débil contra:_____________"<< weakVs[0] << " y " << weakVs[1] << endl;
}

void Zapdos::atk1(Pokemon &other){
	cout << "Zapdos utiliza Peck ! " << endl;//35 de daño
	other.setHP(other.getHP()-35);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}


void Zapdos::atk2(Pokemon &other){
	cout << "Zapdos utiliza Thunder Shock ! " << endl;//40 de daño
	other.setHP(other.getHP()-40);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}

void Zapdos::atk3(Pokemon &other){
	cout << "Zapdos utiliza Drill Peck ! " << endl;//80 de daño
	other.setHP(other.getHP()-80);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}

void Zapdos::atk4(Pokemon &other){
	cout << "Zapdos utiliza Thunderbolt ! " << endl;//90 de daño
	other.setHP(other.getHP()-90);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}