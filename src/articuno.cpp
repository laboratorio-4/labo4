#include "articuno.h"

using namespace std;

Articuno::Articuno() : Pokemon { "Articuno","Legendario",90,85,100,95,125,85,261,"Laaa~ Laaaa~! BFFFFT" }{
	
	type[0]=Volador::type();
	type[1]=Hielo::type();

	strongVs[0]=Volador::strongVs();
	strongVs[1]=Hielo::strongVs();

	weakVs[0]=Volador::weakVs();
	weakVs[1]=Hielo::weakVs();
}

void Articuno::print(){
	Pokemon::printInfo();
	cout << "Tipos:_______________________" << type[0] << " - "<< type[1] << endl;
	cout << "Es fuerte contra:____________" << strongVs[0] << " y " << strongVs[1] << endl;
	cout << "Es débil contra:_____________"<< weakVs[0] << " y " << weakVs[1] << endl;
}



void Articuno::atk1(Pokemon &other){
	cout << "Articuno utiliza Blizzard ! " << endl;//55 de daño
	other.setHP(other.getHP()-55);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}


void Articuno::atk2(Pokemon &other){
	cout << "Articuno utiliza Ice Beam ! " << endl;//90 de daño
	other.setHP(other.getHP()-90);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}

void Articuno::atk3(Pokemon &other){
	cout << "Articuno utiliza Sky Attack ! " << endl;//70 de daño
	other.setHP(other.getHP()-70);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}

void Articuno::atk4(Pokemon &other){
	cout << "Articuno utiliza Ice Shard ! " << endl;//40 de daño
	other.setHP(other.getHP()-40);
	cout << "La salud del enemigo se redujo a: " <<other.getHP() << " ! " << endl<<endl<<endl;
}