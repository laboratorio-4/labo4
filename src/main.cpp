#include "pokemon.h"
#include "articuno.h"
#include "zapdos.h"
#include "moltres.h"

using namespace std;

int main(int argc, char const *argv[])
{
	Articuno *pokemon1 = new Articuno();
	Zapdos *pokemon2 = new Zapdos();
	Moltres *pokemon3 = new Moltres();

	cout <<endl;
	pokemon1->print();
	cout <<endl;

	cout <<endl;
	pokemon2->print();
	cout <<endl;

	cout <<endl;
	pokemon3->print();
	cout <<endl<<endl;


	cout<<" > < > < > Presione ENTER para simular una batalla Pokémon ! < > < > <";
	getchar();
	cout<<endl<<endl;

	int seleccion;

	cout<<endl<<"Elija la batalla a simular: " <<endl<<endl;
    cout<<"(1) Zapdos  vs Moltres" << endl;
    cout<<"(2) Moltres vs Articuno" << endl;
    cout<<"(3) Zapdos  vs Articuno" << endl<<endl;
    cin >> seleccion;
    cout<<endl;

    switch (seleccion) {
        case 1: cout << "Moltres ataca: "<< endl;
				pokemon3->atk1(*pokemon2);
				cout << "Zapdos ataca: " <<endl;
				pokemon2->atk1(*pokemon3);
			break;
		case 2: cout << "Moltres ataca: "<< endl;
				pokemon3->atk3(*pokemon1);
				cout << "Articuno ataca: " << endl;
				pokemon1->atk3(*pokemon3);
			break;
		case 3: cout << "Zapdos ataca: " << endl;
				pokemon2->atk2(*pokemon1);
				cout << "Articuno ataca: " << endl;
				pokemon1->atk1(*pokemon2); 
        	break;
        default: cout << "Opción no válida" << endl;
    } //Fin de switch




	
	return 0;
}

//Falta: cambiar los stats y los ataques
//invocar los call()
