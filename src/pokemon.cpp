#include "pokemon.h"

using namespace std;

Pokemon::Pokemon(){}
Pokemon::~Pokemon(){}
Pokemon::Pokemon(string nombre, string especie, int HP, int ATK, int DEF, int sATK, int sDEF, int SPD, int EXP, string call){

	this-> nombre = nombre;
	this-> especie = especie;
	this-> HP = HP;
	this-> ATK = ATK;
	this-> DEF = DEF;
	this-> sATK = sATK;
	this-> sDEF = sDEF;
	this-> SPD = SPD;
	this-> EXP = EXP;
	this-> call = call;
}

string Pokemon::rugido(){return this->call;}

void Pokemon::printInfo(){
	cout<<"Nombre del pokemon:__________"<< nombre <<endl;
	cout<<"Especie:_____________________"<< especie <<endl;
	cout<<"Puntos de salud:_____________"<< HP <<endl;
	cout<<"Puntos de ataque:____________"<< ATK <<endl;
	cout<<"Puntos de defensa:___________"<< DEF <<endl;
	cout<<"Puntos de ataque especial:___"<< sATK <<endl;
	cout<<"Puntos de defensa especial:__"<< sDEF <<endl;
	cout<<"Velocidad:___________________"<< SPD <<endl;
	cout<<"Experiencia obtenida:________"<< EXP <<endl;
	cout<<"Rugido:______________________"<< call <<endl;
}

int Pokemon::getHP(){return this->HP;}
void Pokemon::setHP(int HP){this->HP=HP;}