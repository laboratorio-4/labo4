#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file electrico.h
 * @brief En este archivo se definen los métodos y atributos de la clase Electrico.
 */

#include "pokemon.h"

using namespace std;

/**
 * @class Electrico
 * @brief La clase Electrico hereda de la clase Pokemon.
 * Se ejecuta el constructor, destructor y las tres funciones estaticas que
 * describen el tipo de pokemon, su fortaleza y debilidad.
 */
 

class Electrico : virtual public Pokemon{
public:
	/**
      * @brief Constructor.
      */ 
	Electrico();
	/**
      * @brief Destructor.
      */ 
	~Electrico();
	/**
      * @brief Funcion estatica que define el tipo de pokemon.
      */ 
	static string type();
	/**
      * @brief Funcion estatica que define ela fortaleza del pokemon.
      */ 
	static string strongVs();
	/**
      * @brief Funcion estatica que define la debilidad del pokemon.
      */ 
	static string weakVs();
	
};