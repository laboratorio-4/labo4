#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file zapdos.h
 * @brief En este archivo se definen los métodos y atributos de la clase Zapdos.
 */


#include "pokemon.h"
#include "electrico.h"
#include "volador.h"

using namespace std;

/**
 * @class Zapdos
 * @brief La clase Zapdos hereda de la clase Pokemon, clase Electrico y clase Volador.
 * Se ejecuta el constructor, la funcion que imprime los datos del pokemon
 * y las 4 funciones virtuales.
 */
 

class Zapdos : public Volador, public Electrico{
public:
	/**
      * @brief Constructor.
      */   
	Zapdos();
	/**
      * @brief Destructor.
      */  
	~Zapdos();
	 /**
      * @brief Imprime la informacion del Pokemon.
      */ 
	void print();
	
	/**
      * @brief Funcion virtual del primer ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk1(Pokemon &other);

	/**
      * @brief Funcion virtual del segundo ataque
	  * @param other objeto de entrada, por referencia.
      */
	void atk2(Pokemon &other);

	/**
      * @brief Funcion virtual del tercer ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk3(Pokemon &other);

	/**
      * @brief Funcion virtual del cuarto ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk4(Pokemon &other);

private:
	string type[2]; 
	string strongVs[2];
	string weakVs[2];
	
};