#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file volador.h
 * @brief En este archivo se definen los métodos y atributos de la clase Volador.
 */

#include "pokemon.h"

using namespace std;

/**
 * @class Volador
 * @brief La clase Volador hereda de la clase Pokemon.
 * Se ejecuta el constructor, destructor y las tres funciones estaticas que
 * describen el tipo de pokemon, su fortaleza y debilidad.
 */

class Volador : virtual public Pokemon{
public:
	/**
      * @brief Constructor.
      */ 
	Volador();
	/**
      * @brief Destructor.
      */ 
	~Volador();
	/**
      * @brief Funcion estatica que define el tipo de pokemon.
      */ 

	static string type();
	/**
      * @brief Funcion estatica que define ela fortaleza del pokemon.
      */ 
	static string strongVs();
	/**
      * @brief Funcion estatica que define la debilidad del pokemon.
      */ 

	static string weakVs();
	
};