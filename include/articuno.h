#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file articuno.h
 * @brief En este archivo se definen los métodos y atributos de la clase Articuno.
 */


#include "pokemon.h"
#include "hielo.h"
#include "volador.h"

using namespace std;

/**
 * @class Articuno
 * @brief La clase Articuno hereda de la clase Pokemon, clase Hielo y clase Volador.
 * Se ejecuta el constructor, la funcion que imprime los datos del pokemon
 * y las 4 funciones virtuales.
 */

class Articuno : public Volador, public Hielo{
public:

	/**
      * @brief Constructor.
      */   
	Articuno();

	/**
      * @brief Destructor.
      */  
	~Articuno();

	 /**
      * @brief Imprime la informacion del Pokemon.
      */ 
	void print();
	/**
      * @brief Funcion virtual del primer ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk1(Pokemon &other);

	/**
      * @brief Funcion virtual del segundo ataque
	  * @param other objeto de entrada, por referencia.
      */ 	
	void atk2(Pokemon &other);

	/**
      * @brief Funcion virtual del tercer ataque
	  * @param other objeto de entrada, por referencia.
      */ 	
	void atk3(Pokemon &other);

	/**
      * @brief Funcion virtual del cuarto ataque
	  * @param other objeto de entrada, por referencia.
      */ 	
	void atk4(Pokemon &other);


private:
	string type[2]; 
	string strongVs[2];
	string weakVs[2];
};