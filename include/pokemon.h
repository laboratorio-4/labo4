#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file pokemon.h
 * @brief En este archivo se definen los métodos y atributos de la clase Pokemon.
 */

#include <iostream>
#include <string>


using namespace std;

/**
 * @class Pokemon
 * @brief La clase Pokemon representa la clase base del programa.
 * Se ejecuta el constructor, la funcion que imprime los datos
 * y las 4 funciones virtuales. Tambien se colocan los atributos de dicha clase
 */

class Pokemon{

public:
/**
      * @brief Constructor por defecto
      */   
	Pokemon();

	/**
      * @brief Constructor personalizado. Define valores para los distintos atributos del Pokémon
      */  
	Pokemon(string nombre, string especie, int HP, int ATK, int DEF, int sATK, int sDEF, int SPD, int EXP, string rugido);
	
	/**
      * @brief Destructor.
      */   
	~Pokemon();

	/**
      * @brief Funcion virtual del primer ataque
      */   
	virtual void atk1(Pokemon &other)=0;

	/**
      * @brief Funcion virtual del segundo ataque
      */  
	virtual void atk2(Pokemon &other)=0;

	/**
      * @brief Funcion virtual del tercer ataque
      */  
	virtual void atk3(Pokemon &other)=0;

	/**
      * @brief Funcion virtual del cuarto ataque
      */   
	virtual void atk4(Pokemon &other)=0;

	/**
      * @brief Funcion que devuelve el atributo call()
      */ 
	string rugido();

	/**
      * @brief Funcion que imprime los datos del pokemon.
      */   
	void printInfo();

	/**
      * @brief Devuelve el valor de ataque del Pokemon
      */ 
	int getATK();

	/**
      * @brief Devuelve el valor de salud del Pokemon
      */ 
	int getHP();

	/**
      * @brief Define la salud del Pokemon 
      */ 
	void setHP(int HP);
	

private:
	string nombre;
	string especie;
	int HP;
	int ATK;
	int DEF;
	int sATK;
	int sDEF;
	int SPD;
	int EXP;
	string call;
};