#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file moltres.h
 * @brief En este archivo se definen los métodos y atributos de la clase Moltres.
 */

#include "pokemon.h"
#include "fuego.h"
#include "volador.h"

using namespace std;

/**
 * @class Moltres
 * @brief La clase Moltres hereda de la clase Pokemon, clase Fuego y clase Volador.
 * Se ejecuta el constructor, la funcion que imprime los datos del pokemon
 * y las 4 funciones virtuales.
 */

class Moltres : public Volador, public Fuego{
public:
	 /**
      * @brief Constructor.
      */    
	Moltres();
	 /**
      * @brief Destructor.
      */    
	~Moltres();

	 /**
      * @brief Imprime la informacion del Pokemon.
      */ 
	void print();

	/**
      * @brief Funcion virtual del primer ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk1(Pokemon &other);

	/**
      * @brief Funcion virtual del segundo ataque
	  * @param other objeto de entrada, por referencia.
      */
	void atk2(Pokemon &other);

	/**
      * @brief Funcion virtual del tercer ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk3(Pokemon &other);

	/**
      * @brief Funcion virtual del cuarto ataque
	  * @param other objeto de entrada, por referencia.
      */ 
	void atk4(Pokemon &other);

private:
	string type[2]; 
	string strongVs[2];
	string weakVs[2];
		
};