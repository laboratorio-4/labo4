#pragma once

/**
 * @author Alexander Calderon Torres
 * @author Roberto Acevedo MOra
 * @date 29/01/2020
 *
 * @file fuego.h
 * @brief En este archivo se definen los métodos y atributos de la clase Fuego.
 */

#include "pokemon.h"

using namespace std;

/**
 * @class Fuego
 * @brief La clase Fuego hereda de la clase Pokemon.
 * Se ejecuta el constructor, destructor y las tres funciones estaticas que
 * describen el tipo de pokemon, su fortaleza y debilidad.
 */
 

class Fuego : virtual public Pokemon
{
public:
	/**
      * @brief Constructor.
      */ 
	Fuego();
	/**
      * @brief Destructor.
      */ 
	~Fuego();
	/**
      * @brief Funcion estatica que define el tipo de pokemon.
      */ 

	static string type();
	/**
      * @brief Funcion estatica que define ela fortaleza del pokemon.
      */ 
	static string strongVs();
	/**
      * @brief Funcion estatica que define la debilidad del pokemon.
      */ 
	static string weakVs();
	
};