# Laboratorio 4: Herencia y Polimorfismo en C++

## Estudiantes:
```
Roberto Acevedo Mora
Alexander Calderón Torres
```

## Instrucciones de compilación:
Una vez descargado el código, ubíquese en la respectiva carpeta:
```
>>cd {PATH}/lab4
```
Seguidamente, ejecute el make:
```
>>make
```

## Para ejecutar el programa:
Para ver los stats de Articunos, Zapdos y Moltres:
```
./bin/pokemon
```

## Para generar la documentación de Doxygen:
Ubíquese en la carpeta principal:
```
>>cd {PATH}/lab4
```

Compile el archivo Doxyfile:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd docs/latex
>>make
```
Esto generará el archivo refman.pdf con la documentación generada por doxygen.
